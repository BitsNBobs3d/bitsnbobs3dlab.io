require 'rubygems'
require 'sitemap_generator'
SitemapGenerator::Sitemap.public_path = 'public/'
SitemapGenerator::Sitemap.default_host = 'http://bitsnbobs.tech'
SitemapGenerator::Sitemap.create do
  add '/404', :changefreq => 'weekly'
  add '/admin', :changefreq => 'weekly'
  add '/blog', :changefreq => 'daily'
  add '/contact', :changefreq => 'weekly'
  add '/metrics', :changefreq => 'weekly'
  add '/order', :changefreq => 'weekly'
  add '/portfolio', :changefreq => 'weekly'
  add '/story', :changefreq => 'weekly'
end

SitemapGenerator::Sitemap.ping_search_engines # Not needed if you use the rake tasks