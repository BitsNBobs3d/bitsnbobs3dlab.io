# bitsnbobs3d.gitlab.io


## Pre-requisites

- Ruby 3.2.2
- RubyGems 3.4.10
- Nanoc 4.12.15
- Python 3.11.3
- Bundler 2.3.26

## SETUP

This project is set up to work on Windows 10 (UPDATE: Dev environment confirmed working on Ubuntu). I found a fix for the lack of `fork()` on Windows, using Threading. However, this takes editing some gem source files, which can be found in `/source`. For ease, run `forkfix.bat`

Also, I got sick of a certain issue with the asdf gem not displaying `index.html` by default. So the python http.server module is the goto.
UPDATE: This python fix is only relevant to Windows development. The command `bundle exec nanoc live` now serves correctly on Ubuntu. The default port is 3000.



##To set up this project


###Windows development
```
gem install bundler
bundle install
source/forkfix.bat
```

###Unix development
```
sudo apt update
sudo apt upgrade
sudo apt-get install ruby-full ruby-bundler python3 git-core zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev

export GEM_HOME=$HOME/.gem/

gem install bundler -g
gem install nanoc -g
bundle install
```

##To run development environment

###Terminal 1
```
bundle exec nanoc live
```

###Terminal 2 (only required for development on Windows)
```
cd public/
python -m http.server
```


##Notes

-adsf 1.4.8 is the default, however there's an issue, so this project is set up with 1.5.0