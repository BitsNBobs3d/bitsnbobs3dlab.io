echo "Windows 10 ONLY!"
echo "fixes an issue relevant to windows 10 with Rubygems, wherein the asdf gem calls a function not available"

del /F "C:\Ruby32-x64\lib\ruby\gems\3.2.0\gems\adsf-1.4.6\lib\adsf\rack\index_file_finder.rb"

del /F "C:\Ruby32-x64\lib\ruby\gems\3.2.0\gems\nanoc-live-1.0.0\lib\nanoc\live\live_recompiler.rb"

copy "index_file_finder.rb" "C:\Ruby32-x64\lib\ruby\gems\3.2.0\gems\adsf-1.4.6\lib\adsf\rack\index_file_finder.rb"

copy "live_recompiler.rb" "C:\Ruby32-x64\lib\ruby\gems\3.2.0\gems\nanoc-live-1.0.0\lib\nanoc\live\live_recompiler.rb"